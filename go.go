/***
 Go语言基础入门
 ***/

package main
import "fmt"
import "time"


func assignment(){
	int_v := 1   //a can't be declared before
	var b, c int //standart declaration
	a := 4  
	a += 1
	b = 2
	c = b
	fmt.Println(b, c, &b, &c, *(&b), *(&c))
	b += 1
	fmt.Println(b, c, &b, &c)

	// uint8, uint16, uint32, uint64
	// int8, int16, int32, int64
	// float32, float64
	// complex64, complex128
	// byte, rune, uintptr, int, uint
	var bool_v bool = true
	var string_v = "hahaha"    // you may ignore the type let the cmp doit
	var char_v rune = 'a'
	var float_v = 12.88

	fmt.Println(int_v, b, c, string_v, char_v, bool_v, float_v)

	if a == b {
		fmt.Println("a==b")
	}else{
		fmt.Println("a!=b")
	}
	fmt.Println(a, b)
}

func main() {
	fmt.Println("now, let's check control out")
	str := "hahahaha"
	lenth := len(str)
	switch lenth {
		case 5:
			fmt.Println(5)
		case 2, 6, 8:
			fmt.Println(8)
		default:
			fmt.Println("unkonwn")
	}

	switch {
		case lenth <= 5:
			fmt.Println("lenth <= 5")
		case 5 < lenth && lenth <= 8:
			fmt.Println("5 < lenth <=8")
		case lenth > 8:
			fmt.Println("lenth > 8")
	}

	//can this test any value?
	var x interface{}
	switch x.(type) {
		case nil:
			fmt.Println("nil")
		case int:
			fmt.Println("its int")
		case string:
			fmt.Println("its string")
	}
	/*
	i := 7
	//select is a block operation untill some case be true
	select {
		case  i = <- 1:
			print("i==7")
	}
	*/

	for i := 0; i<=5; i++ {
		print(i)
		fmt.Println(time.Now())
		print("\n")
	}
	/*
	for {
		this is a dead loop
	}
	*/
	j := 10
	for j >= 0 {
		print(j)
		j -= 1 
	}

	// var array_v = [3]int{1, 33}
	// var array_v1 = [10]string{"a", "b", "c"}
	array_v2 := [7]string{"e", "d", "f"}
	for idx, val := range array_v2 {
		print(idx)
		print("----")
		print(val)
		print("\n")
	}

	



}








